package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.kuzin.tm.api.service.IProjectService;
import ru.kuzin.tm.dto.soap.*;

@Endpoint
public class ProjectCollectionSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectCollectionSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.kuzin.ru/dto/soap";

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectsFindAllRequest", namespace = NAMESPACE)
    public ProjectsFindAllResponse findCollection(@RequestPayload final ProjectsFindAllRequest request) {
        return new ProjectsFindAllResponse(projectService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsSaveRequest", namespace = NAMESPACE)
    public ProjectsSaveResponse saveCollection(@RequestPayload final ProjectsSaveRequest request) {
        projectService.saveAll(request.getProjects());
        return new ProjectsSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsUpdateRequest", namespace = NAMESPACE)
    public ProjectsUpdateResponse updateCollection(@RequestPayload final ProjectsUpdateRequest request) {
        projectService.saveAll(request.getProjects());
        return new ProjectsUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsDeleteRequest", namespace = NAMESPACE)
    public ProjectsDeleteResponse deleteCollection(@RequestPayload final ProjectsDeleteRequest request) {
        projectService.removeAll(request.getProjects());
        return new ProjectsDeleteResponse();
    }

}